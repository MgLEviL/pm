package net.sekao.superbandicoot;

import com.badlogic.gdx.Game;
import net.sekao.superbandicoot.MainScreen;

public class SuperBandicoot extends Game {
        @Override
	public void create() {
		this.setScreen(new MainScreen());
	}
}
