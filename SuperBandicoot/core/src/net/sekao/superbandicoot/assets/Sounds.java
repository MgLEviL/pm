/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 *
 * @author mglevil
 */
public class Sounds {  
    // Musica
    public static Music fondo = Gdx.audio.newMusic(Gdx.files.internal("rain.mp3"));
    // Efectos de sonido
    public static Sound meteorito = Gdx.audio.newSound(Gdx.files.internal("explosion.mp3"));
    // MÃºsica
    public static Music gameover = Gdx.audio.newMusic(Gdx.files.internal("game-over.mp3"));
    // Efectos de sonido
    public static Sound colision = Gdx.audio.newSound(Gdx.files.internal("impact.mp3"));
    // MÃºsica
    public static Sound disparo = Gdx.audio.newSound(Gdx.files.internal("ray_gun.mp3")); 
    // Efectos de sonido
    public static Sound vida = Gdx.audio.newSound(Gdx.files.internal("heal.mp3"));

    public static void pause() {
        fondo.dispose();
        colision.dispose();
        disparo.dispose();
        vida.dispose();
        meteorito.dispose();
        gameover.dispose();
    }

    public static void resume() {
        fondo = Gdx.audio.newMusic(Gdx.files.internal("rain.mp3"));
        
        meteorito = Gdx.audio.newSound(Gdx.files.internal("explosion.mp3"));
            // MÃºsica
        gameover = Gdx.audio.newMusic(Gdx.files.internal("game-over.mp3"));

        // Efectos de sonido
        colision = Gdx.audio.newSound(Gdx.files.internal("impact.mp3"));
            // MÃºsica
        disparo = Gdx.audio.newSound(Gdx.files.internal("ray_gun.mp3"));

        // Efectos de sonido
        vida = Gdx.audio.newSound(Gdx.files.internal("heal.mp3"));
    }
    
}
