/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

/**
 *
 * @author mglevil
 */
public class Textures {   
        
    public static Texture NAVE = new Texture(Gdx.files.internal("spaceShips_005.png"));
    public static Texture PEPINASO = new Texture(Gdx.files.internal("spaceEffects_004.png"));
    public static Texture ENEMY = new Texture(Gdx.files.internal("spaceShips_008.png"));
    public static Texture GAME_OVER = new Texture(Gdx.files.internal("gameover.png"));
    public static Texture FONDO_MENU = new Texture(Gdx.files.internal("fondo_menu.png"));
    public static Texture DISP_ENEMY = new Texture(Gdx.files.internal("spaceEffects_002.png"));
    public static Texture METEORITO = new Texture(Gdx.files.internal("spaceMeteors_001.png"));
    public static Texture FONDO = new Texture(Gdx.files.internal("blackholev.png"));
    public static Texture SAT = new Texture(Gdx.files.internal("spaceBuilding_014.png"));

    public static void pause() {
        NAVE.dispose();
        PEPINASO.dispose();
        ENEMY.dispose();
        GAME_OVER.dispose();
        SAT.dispose();
        FONDO_MENU.dispose();
        DISP_ENEMY.dispose();
        FONDO.dispose();
        METEORITO.dispose();
    }

    public static void resume() {
        PEPINASO = new Texture(Gdx.files.internal("spaceEffects_004.png"));
        ENEMY = new Texture(Gdx.files.internal("spaceShips_008.png"));
        GAME_OVER = new Texture(Gdx.files.internal("gameover.png"));
        FONDO_MENU = new Texture(Gdx.files.internal("fondo_menu.png"));
        DISP_ENEMY = new Texture(Gdx.files.internal("spaceEffects_002.png"));
        METEORITO = new Texture(Gdx.files.internal("spaceMeteors_001.png"));
        FONDO = new Texture(Gdx.files.internal("blackholev.png"));
        SAT = new Texture(Gdx.files.internal("spaceBuilding_014.png"));
    }


}
