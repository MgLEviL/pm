package com.example.mglevil.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final Button bt0 = findViewById(R.id.bt0);
        final Button bt1 = findViewById(R.id.bt1);
        final Button bt2 = findViewById(R.id.bt2);
        final Button bt3 = findViewById(R.id.bt3);
        final Button bt4 = findViewById(R.id.bt4);
        final Button bt5 = findViewById(R.id.bt5);
        final Button bt6 = findViewById(R.id.bt6);
        final Button bt7 = findViewById(R.id.bt7);
        final Button bt8 = findViewById(R.id.bt8);
        final Button bt9 = findViewById(R.id.bt9);

        final Button btcoma = findViewById(R.id.btcoma);
        final Button btclear = findViewById(R.id.btclear);
        final Button btdiv = findViewById(R.id.btdiv);
        final Button btmulti = findViewById(R.id.btmulti);
        final Button btrest = findViewById(R.id.btrest);
        final Button btsum = findViewById(R.id.btsum);
        final Button bteq = findViewById(R.id.bteq);

        final EditText vista = findViewById(R.id.vista);
        final EditText result = findViewById(R.id.result);

        bt0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vista.append(bt0.getText());
            }
        });
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vista.append(bt1.getText());
            }
        });

        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vista.append(bt2.getText());
            }
        });

        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vista.append(bt3.getText());
            }
        });

        bt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vista.append(bt4.getText());
            }
        });

        bt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vista.append(bt5.getText());
            }
        });

        bt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vista.append(bt6.getText());
            }
        });

        bt7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vista.append(bt7.getText());
            }
        });

        bt8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vista.append(bt8.getText());
            }
        });

        bt9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vista.append(bt9.getText());
            }
        });

        btclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!vista.getText().toString().isEmpty()) {
                    vista.getText().delete(vista.length() - 1, vista.length());
                }
            }
        });

        btclear.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                vista.getText().delete(0, vista.length());
                result.getText().delete(0, result.length());
                return true;
            }
        });

        btdiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vista.length() > 0 && !vista.getText().toString().endsWith("+") && !vista.getText().toString().endsWith("-")
                        && !vista.getText().toString().endsWith("x") && !vista.getText().toString().endsWith("/")){
                    vista.append(btdiv.getText());
                }
            }
        });

        btmulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vista.length() > 0 && !vista.getText().toString().endsWith("+") && !vista.getText().toString().endsWith("-")
                        && !vista.getText().toString().endsWith("x") && !vista.getText().toString().endsWith("/")){
                    vista.append(btmulti.getText());
                }
            }
        });

        btrest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vista.length() > 0 && !vista.getText().toString().endsWith("+") && !vista.getText().toString().endsWith("-")
                        && !vista.getText().toString().endsWith("x") && !vista.getText().toString().endsWith("/")){
                    vista.append(btrest.getText());
                }
            }
        });

        btsum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (vista.length() > 0 && !vista.getText().toString().endsWith("+") && !vista.getText().toString().endsWith("-")
                            && !vista.getText().toString().endsWith("x") && !vista.getText().toString().endsWith("/")){
                        vista.append(btsum.getText());
                    }
            }
        });

        btcoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i = 0; i <= vista.length(); i++){
                    if(vista.length() > 0 && !vista.getText().toString().contains(",")){
                        vista.append(btcoma.getText());
                    }
                }

            }
        });

        bteq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(!vista.getText().toString().isEmpty()) {
                        String res;
                        String cadena = vista.getText().toString();
                        String A[] = new String[100];
                        String operaciones[] = new String[100];
                        int pos = 0;
                        int sum = 0;
                        String aux = "";

                        //inicializar operadores.
                        operaciones[0] = "+";
                        int index_operacion = 1;
                        for (int i = 0; i < cadena.length(); i++) {
                            if (cadena.charAt(i) == '+' || cadena.charAt(i) == '-'
                                    || cadena.charAt(i) == 'x' || cadena.charAt(i) == '/') {
                                A[pos] = aux;
                                operaciones[index_operacion] = String.valueOf(cadena.charAt(i));
                                pos++;
                                index_operacion++;
                                aux = "";
                            } else {
                                aux = aux + cadena.charAt(i);
                            }
                        }
                        A[pos] = aux;
                        pos++;

                        for (int i = 0; i < pos; i++) {
                            //Determinar la operacion
                            if (operaciones[i].equals("+")) {
                                sum = sum + Integer.parseInt(A[i]);
                            } else if (operaciones[i].equals("-")) {
                                sum = sum - Integer.parseInt(A[i]);
                            } else if (operaciones[i].equals("x")) {
                                sum = sum * Integer.parseInt(A[i]);
                            } else if (operaciones[i].equals("/")) {
                                sum = sum / Integer.parseInt(A[i]);
                            }
                        }
                        res = Integer.toString(sum);
                        result.append(res);
                        vista.getText().delete(0, vista.length());
                    }
                }catch(ArithmeticException ed){
                    result.append("No divisible, pulse CE");
                }
                catch(Exception e){
                    result.append("Error, Pulse CE");
                }
            }
        });




    }



}
