/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game.elements;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mygdx.game.Naves;
import com.mygdx.game.assets.Sounds;
import com.mygdx.game.assets.Textures;

/**
 *
 * @author mglevil
 */
public class Nave extends Actor{
    private int cont_vidas = 3;
    Rectangle nave;
    private Textures img;
    private Sounds sound;
    private int velocidad;
    private Naves game;
    
    public Nave(Naves gam){
        super();
        this.game = gam;
        img = new Textures();
        
        this.nave = new Rectangle();
        this.nave.x = 368;
        this.nave.y = 20;
        this.nave.width = 64;
        this.nave.height = 64;  

    }
    
    public void crearElement(){
        this.nave = new Rectangle();
        this.nave.x = 368;
        this.nave.y = 20;
        this.nave.width = 64;
        this.nave.height = 64;  
    }
    public void draw(){
        game.batch.draw(img.nave, this.nave.x, this.nave.y,  nave.getWidth(), nave.getHeight()); 
    }

    
    public void checkPosi(){
        if (this.nave.x < 0) {
            this.nave.x = 0;
        }
        if (this.nave.x > 800) {
            this.nave.x = 800;
        }
    }

    public int getCont_vidas() {
        return cont_vidas;
    }

    public void setCont_vidas(int cont_vidas) {
        this.cont_vidas = cont_vidas;
    }

}
