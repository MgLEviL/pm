/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game.elements;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.assets.Sounds;
import com.mygdx.game.assets.Textures;
import java.util.Iterator;

/**
 *
 * @author mglevil
 */
public class EnemyShot extends Actor{


    Rectangle enemyShot;
    Array<Rectangle> enemy_shotS;
    private long tiempo;
    private Textures img;
    private Sounds sound;
    private Enemy enemy;
 
    
    public EnemyShot(Enemy enemy){
        super();    
        enemy_shotS = new Array<Rectangle>();
        sound = new Sounds();
        this.enemy = enemy;
        
        this.enemyShot = new Rectangle();
        this.enemyShot.x = enemy.getX() + 20;
        this.enemyShot.y = 400;
        this.enemyShot.width = 27;
        this.enemyShot.height = 46;
        this.enemy_shotS.add(this.enemyShot);
        this.tiempo = TimeUtils.nanoTime();
        
    }
    
    public void draw(SpriteBatch batch, float parentAlpha){
        Iterator ites = enemy_shotS.iterator();
        
        batch.begin();
       
        while(ites.hasNext()) {
            this.enemyShot = (Rectangle)ites.next();
            batch.draw(img.disp_enemy, enemyShot.x, enemyShot.y);
        }
        batch.end();
    }

public void crearElement(){
    
}

    public void checkTimeElement() {
        if (TimeUtils.nanoTime() - this.tiempo > 500000000) crearElement(); 
    }

    public void checkAndSetElement(Rectangle nave) {
        Iterator iter = enemy_shotS.iterator();
        while(iter.hasNext()) {
            Rectangle disparoEnemy = (Rectangle)iter.next();

            if (disparoEnemy.y + 64 < 0) {
                iter.remove();
            }
            if (disparoEnemy.overlaps(nave)) {
                //cont_vidas--;
                sound.colision.play();
                iter.remove();
            }
        }
    }
    
    
}
