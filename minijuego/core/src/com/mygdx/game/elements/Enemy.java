/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game.elements;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mygdx.game.assets.Sounds;
import com.mygdx.game.assets.Textures;

/**
 *
 * @author mglevil
 */
public class Enemy extends Actor{
    Rectangle enemy;
    private Textures img;
    private Sounds sound;
    private int velocidad;
    private SpriteBatch batch;
    
    public Enemy(){
        super();
        this.enemy = new Rectangle();
        this.enemy.x = 0;
        this.enemy.y = 400;
        this.enemy.height = 64;
        this.enemy.width = 64;
        this.velocidad = 400;


        
        //movimiento
        this.enemy.x += velocidad * Gdx.graphics.getDeltaTime();
        if (enemy.x + enemy.width > 800 || enemy.x < 0) {
            velocidad = -velocidad;
        }
    }
    
    public void draw(SpriteBatch batch, float parentAlpha){
        batch.begin();
        batch.draw(this.img.atacante, enemy.getX(), enemy.getY());
        batch.end();
    }
    
    public void checkPosi(){
        if (this.enemy.x < 0) {
            this.enemy.x = 0;
        }
        if (this.enemy.x > 800) {
            this.enemy.x = 800;
        }
    }
}
