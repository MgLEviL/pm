/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game.elements;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.Naves;
import com.mygdx.game.assets.Sounds;
import com.mygdx.game.assets.Textures;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author mglevil
 */
public class Satelite extends Actor{
    private Naves game;
    Rectangle satelite;
    private Textures img;
    private Sounds sound;
    private int velocidad;
    private long tiempo;
    private Array<Rectangle> satelites;
    private SpriteBatch batch;
    
    public Satelite(Naves gam){
        super();
        this.game = gam;
        
        this.satelites = new Array();
        this.img = new Textures();
        
        this.satelite = new Rectangle();
        this.satelite.x = MathUtils.random(0, 768);
        this.satelite.y = 480;
        this.satelite.height = 32;
        this.satelite.width = 32;
        this.satelites.add(satelite);
        this.tiempo = TimeUtils.nanoTime();
    }

   
    public void draw(){
        Iterator itsat = satelites.iterator();
        Rectangle satelite;
        
        game.batch.begin();
        while(itsat.hasNext()) {
            satelite = (Rectangle)itsat.next();
            game.batch.draw(img.sat, satelite.x, satelite.y, satelite.getWidth(), satelite.getHeight());
        }
        game.batch.end();
    }

    
    public void crearElement() {
        this.satelites = new Array();
        this.img = new Textures();
        
        this.satelite = new Rectangle();
        this.satelite.x = MathUtils.random(0, 768);
        this.satelite.y = 480;
        this.satelite.height = 32;
        this.satelite.width = 32;
        this.satelites.add(satelite);
        this.tiempo = TimeUtils.nanoTime();
    }

    public void checkTimeElement() {
        if ((TimeUtils.nanoTime() - this.tiempo) / 10 > 1000000000) crearElement();
    }

    public void checkAndSetElement(Rectangle nave) {
        Iterator itsat = satelites.iterator();
        velocidad = 100;
        
        while(itsat.hasNext()) {
            Rectangle sat = (Rectangle)itsat.next();
            sat.y -= this.velocidad * Gdx.graphics.getDeltaTime();
            if (sat.y + 32 < 0) {
                itsat.remove();
            }

            if (sat.overlaps(nave)) {
                //cont_vidas++;
                sound.vida.play();
                itsat.remove();
            }
        }
    }    
}
