/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game.elements;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.Naves;
import com.mygdx.game.assets.Sounds;
import com.mygdx.game.assets.Textures;
import java.util.Iterator;

/**
 *
 * @author mglevil
 */
public class Meteorito extends Actor{
    Rectangle meteorito;
    private Textures img;
    private Sounds sound;
    private int velocidad;
    private long tiempo;
    private Array<Rectangle> meteoritos;
    private Naves game;

private SpriteBatch batch;
    public Meteorito(Naves gam){
        this.game = gam;
        this.meteoritos = new Array();
        img = new Textures();
        
        this.meteorito = new Rectangle();
        this.meteorito.x = MathUtils.random(0, 768);
        this.meteorito.y = 480;
        this.meteorito.height = 32;
        this.meteorito.width = 32;
        this.meteoritos.add(meteorito);
        this.tiempo = TimeUtils.nanoTime();
        
    }



    
    public void crearElement() {
        this.meteorito = new Rectangle();
        this.meteorito.x = MathUtils.random(0, 768);
        this.meteorito.y = 480;
        this.meteorito.height = 32;
        this.meteorito.width = 32;
        this.meteoritos.add(meteorito);
        this.tiempo = TimeUtils.nanoTime();
       
    }

    
    public void draw(){
        Iterator itmet = meteoritos.iterator();
        
        game.batch.begin();       
        while(itmet.hasNext()) {
            meteorito = (Rectangle)itmet.next();
            game.batch.draw(img.meteorito, meteorito.x, meteorito.y, meteorito.getWidth(), meteorito.getHeight());
        }
        game.batch.end();
    }

    
    public void checkTimeElement() {
        if ((TimeUtils.nanoTime() - this.tiempo) / 3 > 1000000000) crearElement(); 
    }

    
    public void checkAndSetElement(Rectangle nave) {
        velocidad = 600;
        Iterator itm = meteoritos.iterator();
        while(itm.hasNext()) {
            Rectangle meteorito = (Rectangle)itm.next();
            meteorito.y -= this.velocidad * Gdx.graphics.getDeltaTime();
            
            if (meteorito.y + 32 < 0) {
                itm.remove();
            }
            if (meteorito.overlaps(nave)) {
                sound.meteorito.play();
                //cont_vidas = 0;
                itm.remove();
            }
        }    
    }   

}
