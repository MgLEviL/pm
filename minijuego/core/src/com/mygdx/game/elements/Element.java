/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game.elements;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.assets.Sounds;
import com.mygdx.game.assets.Textures;

/**
 *
 * @author mglevil
 */
public abstract class Element {
    protected Rectangle elemento;
    protected long tiempo;
    protected Sounds sonido;
    protected Textures img;
    protected Array<Rectangle> arrayElemetns;
    protected int velocidad;
    
    public abstract void crearElement();
    public abstract void draw();
    public abstract void checkTimeElement();
    public abstract void checkAndSetElement(Rectangle nave);
    
}
