/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game.elements;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.assets.Sounds;
import com.mygdx.game.assets.Textures;
import java.util.Iterator;

/**
 *
 * @author mglevil
 */
public class Disparo extends Actor{
    private int cont_disparos;
    private int destruidos;
    private Nave nav;
    private Textures img;
    private Rectangle disparo;
    private Sounds sonido;
    private long tiempo;
    private Array<Rectangle> disparos;
    
    public Disparo(Nave nav){
        super();
     
        cont_disparos = 0;
        destruidos = 0;
        img = new Textures();
        disparos = new Array();
        
        
    }

    public void crearElement(){
        if (TimeUtils.nanoTime() - tiempo > 500000000) {
            disparo = new Rectangle();
            disparo.x = nav.getX() + 20;
            disparo.y = nav.getY();
            disparo.width = 28;
            disparo.height = 46;
            disparos.add(disparo);
            tiempo = TimeUtils.nanoTime();
        }
    }

    public void draw(SpriteBatch batch, float parentAlpha){
        Iterator itdis = disparos.iterator();
        batch.begin();
        while(itdis.hasNext()) {
            disparo = (Rectangle)itdis.next();
            batch.draw(img.pepinaso, disparo.x, disparo.y);
        }
        batch.end();
    }
    

    public void checkAndSetElement(Rectangle enemy) {
        Iterator it = disparos.iterator();
        while(it.hasNext()) {
            Rectangle elemento = (Rectangle)it.next();
            elemento.y += 500 * Gdx.graphics.getDeltaTime();
            
            if (elemento.y + 64 > 800) {
                it.remove();
                cont_disparos++;
            }
            if (elemento.overlaps(enemy)) {
                destruidos++;
                sonido.colision.play();
                it.remove();
            }
        }
    }
}
