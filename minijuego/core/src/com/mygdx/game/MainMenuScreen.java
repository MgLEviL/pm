package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.mygdx.game.assets.Textures;

public class MainMenuScreen implements Screen {
    final Naves game;
    OrthographicCamera camera;
    private Textures fondo;

    public MainMenuScreen(Naves gam) {
        this.game = gam;
        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, 800, 480);
        fondo = new Textures();
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2F, 1);
        Gdx.gl.glClear(16384);
        camera.update();
        game.batch.setProjectionMatrix(this.camera.combined);
        game.batch.begin();
        game.batch.draw(fondo.fondo_menu, 0, 0);
        game.font.draw(game.batch, "Bievenido ami juego de naves!!! ", 300, 450);
        game.font.draw(game.batch, "Toque cualquier punto para comenzar!", 300, 400);
        game.batch.end();
        if (Gdx.input.isTouched()) {
            game.setScreen(new GameScreen(game));
            dispose();
        }

    }

    public void resize(int width, int height) {
    }

    public void show() {
    }

    public void hide() {
    }

    public void pause() {
    }

    public void resume() {
    }

    public void dispose() {
        fondo.fondo_menu.dispose();
    }
}
