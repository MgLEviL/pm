/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 *
 * @author mglevil
 */
public class Sounds {
    public Sound disparo;
    public Music fondo;
    public Sound vida;
    public Sound meteorito;
    public Sound colision;    
    public Music gameover;
    
    public Sounds(){
        this.colision = Gdx.audio.newSound(Gdx.files.internal("impact.mp3"));
        this.meteorito = Gdx.audio.newSound(Gdx.files.internal("explosion.mp3"));
        this.vida = Gdx.audio.newSound(Gdx.files.internal("heal.mp3"));
        this.disparo = Gdx.audio.newSound(Gdx.files.internal("ray_gun.mp3"));
        this.fondo = Gdx.audio.newMusic(Gdx.files.internal("rain.mp3"));
        this.gameover = Gdx.audio.newMusic(Gdx.files.internal("game-over.mp3"));
        this.fondo.setLooping(true);
    }
}
