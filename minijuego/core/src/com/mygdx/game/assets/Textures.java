/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

/**
 *
 * @author mglevil
 */
public class Textures {
    public Texture pepinaso;
    public Texture disp_enemy;
    public Texture nave;
    public Texture atacante;
    public Texture meteorito;
    public Texture sat;
    public Texture fondo;
    public Texture gameover;
    public Texture fondo_menu;
    
    
    public Textures(){
        this.sat = new Texture(Gdx.files.internal("spaceBuilding_014.png"));
        this.fondo = new Texture(Gdx.files.internal("blackholev.png"));
        this.meteorito = new Texture(Gdx.files.internal("spaceMeteors_001.png"));
        this.pepinaso = new Texture(Gdx.files.internal("spaceEffects_004.png"));
        this.disp_enemy = new Texture(Gdx.files.internal("spaceEffects_002.png"));
        this.nave = new Texture(Gdx.files.internal("spaceShips_005.png"));
        this.atacante = new Texture(Gdx.files.internal("spaceShips_008.png"));
        this.gameover = new Texture(Gdx.files.internal("gameover.png"));
        this.fondo_menu = new Texture(Gdx.files.internal("fondo_menu.png"));
    }

}
