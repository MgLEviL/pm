package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.mygdx.game.assets.Sounds;
import com.mygdx.game.assets.Textures;

public class FinalScreen implements Screen {
    final Naves game;
    OrthographicCamera camera;
    private Sounds sonido;
    private Textures img;

    public FinalScreen(Naves gam) {
        this.game = gam;
        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, 800.0F, 480.0F);
        sonido = new Sounds();
        img = new Textures();
        sonido.gameover.play();
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(0.0F, 0.0F, 0.2F, 1.0F);
        Gdx.gl.glClear(16384);
        this.camera.update();
        this.game.batch.setProjectionMatrix(this.camera.combined);
        this.game.batch.begin();
        this.game.batch.draw(img.gameover, 0, 0);
        this.game.batch.end();
        
        if (Gdx.input.isTouched()) {
            this.game.setScreen(new MainMenuScreen(this.game));
            this.dispose();
        }

    }

    public void resize(int width, int height) {
    }

    public void show() {
    }

    public void hide() {
    }

    public void pause() {
    }

    public void resume() {
    }

    public void dispose() {
        this.sonido.gameover.dispose();
        this.img.gameover.dispose();
    }
}
