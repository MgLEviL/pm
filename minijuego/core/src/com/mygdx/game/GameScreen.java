package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.assets.Sounds;
import com.mygdx.game.assets.Textures;
import com.mygdx.game.elements.Disparo;
import com.mygdx.game.elements.Enemy;
import com.mygdx.game.elements.EnemyShot;
import com.mygdx.game.elements.Meteorito;
import com.mygdx.game.elements.Nave;
import com.mygdx.game.elements.Satelite;

public class GameScreen implements Screen {
    final Naves game;
    public int cont_vidas = 3;
    private int destruidos;
    private int cont_disparos;
    private SpriteBatch batch;

    private OrthographicCamera camara;
    
    private Sounds sonido;
    private Textures img;
    
    Enemy enemy;
    Nave nave;
    EnemyShot es;
    Meteorito mt;
    Satelite st;
    Disparo shot;

    public GameScreen(Naves gam) {
        this.game = gam;      
        
        this.camara = new OrthographicCamera();
        this.camara.setToOrtho(false, 800, 480);
        img = new Textures();
        sonido = new Sounds();
        
        nave = new Nave(game);
        enemy = new Enemy();
        es = new EnemyShot(enemy);
        mt = new Meteorito(game);
        st = new Satelite(game);
        shot = new Disparo(nave);
        
        nave.crearElement();
        //enemy.crearElement();
        
        mt.crearElement();
        st.crearElement();
        //es.crearElement();
    }
    
    
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2F, 1);
        Gdx.gl.glClear(16384);
        
        camara.update();
        
        game.batch.setProjectionMatrix(camara.combined);
        //moveEnemy();
        
        nave.draw();
        //enemy.draw(batch, delta);
        //es.draw(batch, delta);
        mt.draw();
        st.draw();
        
        //showInfo();
        
        nave.checkPosi();
        //enemy.checkPosi();
        
        es.checkTimeElement();
        mt.checkTimeElement();
        st.checkTimeElement();
        
        
        //lectura de entrada
        if (Gdx.input.isTouched()) {
            Vector3 posicionTocada = new Vector3();
            posicionTocada.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camara.unproject(posicionTocada);
            //nave.getX() = posicionTocada.x - 32;
        }

        /*
        if (Gdx.input.isKeyPressed(Keys.LEFT)) xnave -= 900 * Gdx.graphics.getDeltaTime();   
        if (Gdx.input.isKeyPressed(Keys.RIGHT)) xnave += 900 * Gdx.graphics.getDeltaTime();
        if (Gdx.input.isKeyPressed(Keys.UP)) {
            shot.crearElement();
            sonido.disparo.play();
        }*/

        finJuego();
    }
/*
    private void dificultadProgres(Rectangle disparoEnemy) {
        if (this.destruidos < 5) {
            disparoEnemy.y -= vel_gota * Gdx.graphics.getDeltaTime();
        } else if (destruidos >= 5 && destruidos < 10) {
            vel_gota = 700;
            disparoEnemy.y -= vel_gota * Gdx.graphics.getDeltaTime();
        } else if (destruidos >= 10) {
            this.vel_gota = 1000;
            disparoEnemy.y -= vel_gota * Gdx.graphics.getDeltaTime();
        }

    }*/


    private void finJuego() {
        if (cont_vidas == 0) {
            game.setScreen(new FinalScreen(game));
            dispose();
        }
    }


    @Override
    public void show() {
        sonido.fondo.play();
        
        game.batch.begin();
        game.batch.draw(img.fondo, 0, 0);
        game.font.draw(game.batch, "Intentos: ", 650, 450);
//        game.font.draw(game.batch, Integer.toString(nave.getCont_vidas()), 750, 450);
        game.font.draw(game.batch, "Disparos fallidos: ", 620, 430);
        game.font.draw(game.batch, Integer.toString(this.cont_disparos), 750, 430);
        game.font.draw(game.batch, "Naves destruidas: ", 620, 410);
        game.font.draw(game.batch, Integer.toString(this.destruidos), 750, 410);
        game.batch.end();
    }

    @Override
    public void resize(int i, int i1) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }
    @Override
    public void hide() {
    }
    @Override
    public void dispose() {
        this.img.disp_enemy.dispose();
        this.img.nave.dispose();
        this.img.meteorito.dispose();
        this.img.atacante.dispose();
        this.img.pepinaso.dispose();
        this.img.sat.dispose();
        this.img.fondo.dispose();
        this.sonido.disparo.dispose();
        this.sonido.vida.dispose();
        this.sonido.fondo.dispose();
        this.sonido.meteorito.dispose();
        this.sonido.colision.dispose();
    }
}

